﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BibliotecaDeDatos.Modelos;


namespace BibliotecaDeDatos.Datos
{
    class DDatos
    {
      
        public List<MAlumnos> ListarAlumnos()
        {
            DConexion clsConex = new DConexion();

            String conx = "";
            try
            {
                 conx = clsConex.GetConexion;
            }
            catch (Exception ex)
            {
                string ms = ex.ToString();
            }
            
            List<MAlumnos> lista = new List<MAlumnos>();
            MAlumnos _MAlumnos = new MAlumnos();
            using (SqlConnection conexion = new SqlConnection(conx))
            {
                using (SqlCommand comando = new SqlCommand("Usp_ListadoAlumnos", conexion))
                {
                    comando.CommandType = System.Data.CommandType.StoredProcedure;
                    conexion.Open();
                    SqlDataReader reader = comando.ExecuteReader();
                    while (reader.Read())
                    {
                        _MAlumnos = new MAlumnos();
                        _MAlumnos.iIdAlumno = Convert.ToInt64(reader["iIdAlumno"].ToString().Trim());
                        _MAlumnos.sNombres = reader["vNombres"].ToString().Trim();
                        _MAlumnos.sApellidos = reader["vApellidos"].ToString().Trim();
                        _MAlumnos.sDireccion = reader["vDireccion"].ToString().Trim();
                        _MAlumnos.lTelefono = Convert.ToInt32(reader["iTelefono"].ToString().Trim());
                        _MAlumnos.sDNI = reader["vDNI"].ToString().Trim();
                        _MAlumnos.dFechaNacimiento = Convert.ToDateTime(reader["dFechaNacimiento"].ToString().Trim());
                        _MAlumnos.sSexo = reader["vSexo"].ToString().Trim();
                        lista.Add(_MAlumnos);
                    }
                    conexion.Close();
                    comando.Dispose();
                }
            }
            return lista;
        }







    }
}
