﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.Extensions;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace BibliotecaDeDatos.Datos
{
    public class DConexion
    {
        public String GetConexion
        {
            get
            {
                //var configurationBuilder = new ConfigurationBuilder();
                //string path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
                //configurationBuilder.AddJsonFile(path, false);
                //string cnx = configurationBuilder.Build().GetSection("ConnectionStrings:ConexionBD").Value;
                //return cnx;
                String _value = ConfigurationManager.ConnectionStrings["BD_MATRICULA"].ToString();
                return _value;
            }
        }
    }
}
