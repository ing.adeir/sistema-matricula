﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotecaDeDatos.Modelos
{
    public class MAlumnos
    {
        public long iIdAlumno { set; get; }
        public String sNombres { set; get; }
        public String sApellidos { set; get; }
        public String sDireccion { set; get; }
        public int lTelefono { set; get; }
        public String sDNI { set; get; }
        public DateTime dFechaNacimiento { set; get; }
        public String sSexo { set; get; }
    }
}
