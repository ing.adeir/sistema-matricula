﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BibliotecaDeDatos.Modelos;
using BibliotecaDeDatos.Datos;

namespace BibliotecaDeDatos.Negocios
{
    public class NAlumnos
    {
        public List<MAlumnos> ListarAlumnos()
        {
            List<MAlumnos> lstAlumnos = new List<MAlumnos>();
            DDatos clsDatos = new DDatos();
            lstAlumnos = clsDatos.ListarAlumnos();
            return lstAlumnos;
        }
    }
}
