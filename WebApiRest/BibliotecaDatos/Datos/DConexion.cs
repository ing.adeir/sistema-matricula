﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace BibliotecaDatos.Datos
{
    class DConexion
    {
        public String GetConexion
        {
            get
            {
                String _value = ConfigurationManager.ConnectionStrings["BdDist"].ToString();
                return _value;
            }
        }
    }
}
