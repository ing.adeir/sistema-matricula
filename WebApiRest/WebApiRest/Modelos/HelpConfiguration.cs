﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiRest.Modelos
{
    public class HelpConfiguration
    {
        
        public Boolean CreacionArchivoLog( string texError)
        {
            Boolean resultado = true;
            try
            {
                string path = @"D:\LogSistemaMatricula.txt";
                if (!File.Exists(path))
                {
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine(texError);
                    }
                }
                else
                {                    
                    StreamWriter writer = new StreamWriter(path, append: true);
                    writer.WriteLine(texError);
                    writer.Close();
                }

                //System.IO.File.WriteAllText(@"D:\LogSistemaMatricula.txt", texError);
            }
            catch (Exception ex)
            {
                resultado = false;
            }

            return resultado;


        }
    }
}
