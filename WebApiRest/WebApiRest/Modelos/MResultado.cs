﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiRest.Modelos
{
    public class MResultado
    {
        public Boolean bResultado { get; set; }
        public string sMensaje { get; set; }
    }
}
