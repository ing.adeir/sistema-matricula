﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiRest.Modelos;
using WebApiRest.Negocios;
using WebApiRest.Datos;

namespace WebApiRest.Negocios
{
    public class NAlumnos
    {
        HelpConfiguration help = new HelpConfiguration();

        public List<MAlumnos> ListarAlumnos()
        {
            List<MAlumnos> lstAlumnos = new List<MAlumnos>();
            DDatos clsDatos = new DDatos();
            try
            {
                lstAlumnos = clsDatos.ListarAlumnos();
            }            
            catch(Exception ex){
                help.CreacionArchivoLog(ex.ToString());
            }
            return lstAlumnos;
        }

        public MResultado RegistrarAlumno(MAlumnos _request)
        {
            MResultado _response = new MResultado();
            DDatos clsDatos = new DDatos();
            try
            {
                _response = clsDatos.RegistarAlumno(_request);
            }
            catch (Exception ex)
            {
                help.CreacionArchivoLog(ex.ToString());
            }
            return _response;
        }

        public MResultado ActualizarAlumno(MAlumnos _request)
        {
            MResultado _response = new MResultado();
            DDatos clsDatos = new DDatos();
            try
            {
                _response = clsDatos.ActualizarAlumno(_request);
            }
            catch (Exception ex)
            {
                help.CreacionArchivoLog(ex.ToString());
            }
            return _response;
        }

        public MResultado EliminarAlumno(int _iIdx)
        {
            MResultado _response = new MResultado();
            DDatos clsDatos = new DDatos();
            try
            {
                _response = clsDatos.EliminarAlumno(_iIdx);
            }
            catch (Exception ex)
            {
                help.CreacionArchivoLog(ex.ToString());
            }
            return _response;
        }



    }
}
