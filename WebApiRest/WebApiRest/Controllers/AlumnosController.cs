﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiRest.Modelos;
using WebApiRest.Negocios;

namespace WebApiRest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlumnosController : ControllerBase
    {        
        [HttpGet]
        [Route("listado")] ////Api/Alumnos/listado
        public ActionResult ListadoAlumnos()
        {
            NAlumnos neg = new NAlumnos();
            var resultado = neg.ListarAlumnos();
            //Response.Headers.Add("X-Total-Count", "Exito");
            return Ok(resultado);
        }

        [HttpPost]
        [Route("registrar")]  ////Api/Alumnos/registrar
        public ActionResult Post([FromBody] MAlumnos _request)
        {
            NAlumnos neg = new NAlumnos();
            var resultado = neg.RegistrarAlumno(_request);
            //Response.Headers.Add("X-Total-Count", "Exito");
            return Ok(resultado);
        }

        [HttpPut]
        [Route("actualizar/{id}")] ///Api/Alumnos/actualizar/id
        public ActionResult Put( int id, [FromBody] MAlumnos _request)
        {
            NAlumnos neg = new NAlumnos();
            var resultado = neg.ActualizarAlumno(_request);
            //Response.Headers.Add("X-Total-Count", "Exito");
            return Ok(resultado);
        }

        // DELETE api/values/5
        [HttpDelete]
        [Route("eliminar/{id}")] /// Api/Alumnos/eliminar
        public ActionResult Delete(int id)
        {
            NAlumnos neg = new NAlumnos();
            var resultado = neg.EliminarAlumno(id);
            //Response.Headers.Add("X-Total-Count", "Exito");
            return Ok(resultado);
        }




    }




}