﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebApiRest.Modelos;

namespace WebApiRest.Datos
{
    public class DDatos
    {
        DConexion clsConex = new DConexion();

        public List<MAlumnos> ListarAlumnos()
        {
            String conx = clsConex.GetConexion;
            List<MAlumnos> lista = new List<MAlumnos>();
            MAlumnos _MAlumnos = new MAlumnos();
            using (SqlConnection conexion = new SqlConnection(conx))
            {
                using (SqlCommand comando = new SqlCommand("Usp_ListadoAlumnos", conexion))
                {
                    comando.CommandType = System.Data.CommandType.StoredProcedure;
                    conexion.Open();
                    SqlDataReader reader = comando.ExecuteReader();
                    while (reader.Read())
                    {
                        _MAlumnos = new MAlumnos();
                        _MAlumnos.iIdAlumno = Convert.ToInt64(reader["iIdAlumno"].ToString().Trim());
                        _MAlumnos.sNombres = reader["vNombres"].ToString().Trim();
                        _MAlumnos.sApellidos = reader["vApellidos"].ToString().Trim();
                        _MAlumnos.sDireccion = reader["vDireccion"].ToString().Trim();
                        _MAlumnos.lTelefono = Convert.ToInt32(reader["iTelefono"].ToString().Trim());
                        _MAlumnos.sDNI = reader["vDNI"].ToString().Trim();
                        _MAlumnos.dFechaNacimiento = Convert.ToDateTime(reader["dFechaNacimiento"].ToString().Trim());
                        _MAlumnos.sSexo = reader["vSexo"].ToString().Trim();
                        lista.Add(_MAlumnos);
                    }
                    conexion.Close();
                    comando.Dispose();
                }
            }
            return lista;
        }


        public MResultado RegistarAlumno(MAlumnos _request)
        {
            String conx = clsConex.GetConexion;
            MResultado _response = new MResultado();
            _response.bResultado = false;
            using (SqlConnection conexion = new SqlConnection(conx))
            {
                using (SqlCommand comando = new SqlCommand("Usp_RegistrarAlumno", conexion))
                {
                    comando.CommandType = System.Data.CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@vNombres", _request.sNombres);
                    comando.Parameters.AddWithValue("@vApellidos", _request.sApellidos);
                    comando.Parameters.AddWithValue("@vDireccion", _request.sDireccion);
                    comando.Parameters.AddWithValue("@iTelefono", _request.lTelefono);
                    comando.Parameters.AddWithValue("@vDNI", _request.sDNI);
                    comando.Parameters.AddWithValue("@dFechaNac", _request.dFechaNacimiento);
                    comando.Parameters.AddWithValue("@vSexo", _request.sSexo);                    
                    conexion.Open();
                    comando.ExecuteNonQuery();
                    _response.bResultado = true;
                    _response.sMensaje = "La operación se realizó correctamente!";
                    conexion.Close();
                    comando.Dispose();                    
                }
            }
            return _response;
        }


        public MResultado ActualizarAlumno(MAlumnos _request)
        {
            String conx = clsConex.GetConexion;
            MResultado _response = new MResultado();
            _response.bResultado = false;
            using (SqlConnection conexion = new SqlConnection(conx))
            {
                using (SqlCommand comando = new SqlCommand("Usp_ActualizarAlumno", conexion))
                {
                    comando.CommandType = System.Data.CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id", _request.iIdAlumno);
                    comando.Parameters.AddWithValue("@vNombres", _request.sNombres);
                    comando.Parameters.AddWithValue("@vApellidos", _request.sApellidos);
                    comando.Parameters.AddWithValue("@vDireccion", _request.sDireccion);
                    comando.Parameters.AddWithValue("@iTelefono", _request.lTelefono);
                    comando.Parameters.AddWithValue("@vDNI", _request.sDNI);
                    comando.Parameters.AddWithValue("@dFechaNac", _request.dFechaNacimiento);
                    comando.Parameters.AddWithValue("@vSexo", _request.sSexo);                    
                    conexion.Open();
                    comando.ExecuteNonQuery();
                    _response.bResultado = true;
                    _response.sMensaje = "La operación se realizó correctamente!";
                    conexion.Close();
                    comando.Dispose();
                }
            }
            return _response;
        }


        public MResultado EliminarAlumno(int _iIdx)
        {
            String conx = clsConex.GetConexion;
            MResultado _response = new MResultado();
            _response.bResultado = false;
            using (SqlConnection conexion = new SqlConnection(conx))
            {
                using (SqlCommand comando = new SqlCommand("Usp_EliminarAlumno", conexion))
                {
                    comando.CommandType = System.Data.CommandType.StoredProcedure;
                    comando.Parameters.AddWithValue("@id", _iIdx);
                    conexion.Open();
                    comando.ExecuteNonQuery();
                    _response.bResultado = true;
                    _response.sMensaje = "La operación se realizó correctamente!";
                    conexion.Close();
                    comando.Dispose();
                }
            }
            return _response;
        }






    }
}
