﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiRest.Datos
{
    public class DConexion
    {
        public String GetConexion
        {
            get
            {
                var configurationBuilder = new ConfigurationBuilder();
                string path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
                configurationBuilder.AddJsonFile(path, false);
                string cnx = configurationBuilder.Build().GetSection("ConnectionStrings:BDMatricula").Value;
                return cnx;
            }
        }
    }
}
