﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace BibliotecaData
{
    public class Class1
    {
        public String GetConexion
        {
            get
            {
                var configurationBuilder = new ConfigurationBuilder();
                string path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
                configurationBuilder.AddJsonFile(path, false);
                string cnx = configurationBuilder.Build().GetSection("ConnectionStrings:ConexionBD").Value;
                return cnx;
                //String _value = ConfigurationManager.ConnectionStrings["BD_MATRICULA"].ToString();
                //return _value;
            }
        }
    }
}
