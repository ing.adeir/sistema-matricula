

CREATE DATABASE BD_Matricula;

USE BD_Matricula;


/* TABLAS */

CREATE TABLE Tb_Alumnos(
 iIdAlumno int identity(1,1) primary key,
 vNombres varchar(100),
 vApellidos varchar(100),
 vDireccion varchar(300),
 iTelefono int,
 vDNI varchar(100),
 dFechaNacimiento datetime,
 vSexo varchar(5)
);



select * from Tb_Alumnos;

/* REGISTROS */
INSERT INTO Tb_Alumnos (vNombres, vApellidos, vDireccion, iTelefono, vDNI, dFechaNacimiento, vSexo) VALUES ('Juan','Perez Morales','av. Alcazar 123',987654321,'76876512','02/02/1991','M');
INSERT INTO Tb_Alumnos (vNombres, vApellidos, vDireccion, iTelefono, vDNI, dFechaNacimiento, vSexo) VALUES ('Adeir','Modragon Aguirre','av. Tupac Amaru 123',999654321,'79876512','01/05/1993','M');

/* STORE PROCEDURE */

ALTER PROCEDURE Usp_ListadoAlumnos
AS
BEGIN

	SELECT 
	 iIdAlumno,
	 vNombres,
	 vApellidos,
	 vDireccion,
	 iTelefono,
	 vDNI,
	 dFechaNacimiento,
	 vSexo
	FROM Tb_Alumnos;

END











