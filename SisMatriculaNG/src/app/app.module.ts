import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListadoAlumnosComponent } from './MisComponentes/listado-alumnos/listado-alumnos.component';
import { AlumnosServicioService } from './services/alumnos-servicio.service';
/* Se importa el servicio en providers y HttpModule */
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ModalAlumnoComponent } from './MisComponentes/modal-alumno/modal-alumno.component';
import { ListadoProfesoresComponent } from './MisComponentes/listado-profesores/listado-profesores.component';
import { InicioComponent } from './MisComponentes/inicio/inicio.component'; //Para convertir de string a DATE en el ts
import { appRoutingProviders, routing } from './app.routing';
import { Error404Component } from './MisComponentes/error404/error404.component';

@NgModule({
  declarations: [
    AppComponent,
    ListadoAlumnosComponent,
    ModalAlumnoComponent,
    ListadoProfesoresComponent,
    InicioComponent,
    Error404Component
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    routing
  ],
  providers: [
    AlumnosServicioService,
    DatePipe,
    appRoutingProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
