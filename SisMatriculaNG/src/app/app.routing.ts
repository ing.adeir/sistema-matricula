// Importar modulos de angular
import { ModuleWithProviders } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

// Importar componentes
import { InicioComponent } from './MisComponentes/inicio/inicio.component';
import { ListadoAlumnosComponent } from './MisComponentes/listado-alumnos/listado-alumnos.component';
import { ListadoProfesoresComponent } from './MisComponentes/listado-profesores/listado-profesores.component';
import { Error404Component } from './MisComponentes/error404/error404.component';

// Array de Rutas o componenetes

const appRoutes: Routes = [
    { path: '', component: InicioComponent},
    { path: 'inicio', component: InicioComponent},
    { path: 'alumnos', component: ListadoAlumnosComponent},
    { path: 'profesores', component: ListadoProfesoresComponent},
    { path: '**', component: Error404Component}

]; 


// exportar el modulo de rutas

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);




