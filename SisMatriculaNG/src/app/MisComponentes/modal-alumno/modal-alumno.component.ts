import { Component, OnInit, Output } from '@angular/core';
/* Importacion de componente y otros */
import { NgbActiveModal, NgbDateStruct, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { Alumnos } from '../../models/alumnos.models'
import { Input } from '@angular/core';
import { NgForm} from '@angular/forms';
import { DatePipe } from '@angular/common'
import { NgbDateCustomParserFormatter } from '../../models/dateFormat';
import { AlumnosServicioService } from '../../services/alumnos-servicio.service';

@Component({
  selector: 'app-modal-alumno',
  templateUrl: './modal-alumno.component.html',
  styleUrls: ['./modal-alumno.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}
  ]
})
export class ModalAlumnoComponent implements OnInit {

  @Input() public mAlumno: Alumnos;
  @Input() public sTipoCRUD:string;
  sTituloModal: string;
  sTextoBoton: string;
  dFechaNacimientoValor: NgbDateStruct;
  nuevoAlumno: Alumnos;
  

  constructor(public activeModal: NgbActiveModal, 
              private fechaPipe: DatePipe, 
              private serviceAlumno: AlumnosServicioService
              ) { }

  ngOnInit(): void {        
    if(this.sTipoCRUD=="editar"){
      var fechaNacimiento = this.fechaPipe.transform(this.mAlumno.dFechaNacimiento, "yyyy/MM/dd");
      var fechaNac = fechaNacimiento.split("/");
      var dia = parseInt(fechaNac[2],10);
      var mes = parseInt(fechaNac[1],10);
      var anio = parseInt(fechaNac[0],10);
      var FechangbDateStruct = { day: dia, month: mes, year: anio };    
      this.dFechaNacimientoValor =FechangbDateStruct;    
      this.sTituloModal =  "Editar Alumno: " + this.mAlumno.sNombres + " " + this.mAlumno.sApellidos;  
      this.sTextoBoton = "Actualizar";    
    }else{
      this.sTituloModal ="Nuevo Alumno";
      this.sTextoBoton = "Guardar";
    }
  }
  
  ActualizarAlumno(form: NgForm){        
    console.log(form.value);
    var jsonFecha = form.value.dFechaNacimiento;    
    var dia = jsonFecha.day;
    var mes = jsonFecha.month;
    var anio = jsonFecha.year;
    var dFecha = new Date(anio+"-"+mes+"-"+dia);
    this.nuevoAlumno = form.value;
    this.nuevoAlumno.dFechaNacimiento= dFecha;
    if(this.sTipoCRUD=="editar"){
      this.serviceAlumno.EditarAlumno(this.nuevoAlumno).subscribe((mResultado)=>{
        //console.log(mResultado);
        alert(mResultado.sMensaje);
        this.activeModal.close(mResultado);      
      },(errorObtenido)=>{
        console.log(errorObtenido);
      } );
    }else{
      this.serviceAlumno.GuardarNuevoAlumno(this.nuevoAlumno).subscribe( (mResultado)=>{
        console.log(mResultado);
        alert(mResultado.sMensaje);
        this.activeModal.close(mResultado); 
      },(error)=>{
        console.log(error);
      }
      )
    }
    

  }
  

  

}
