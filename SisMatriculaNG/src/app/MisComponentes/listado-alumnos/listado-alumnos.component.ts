import { Component, OnInit, Input } from '@angular/core';
/* Importacion de librerias para cargar la pagina */
import { Observable, Subscriber } from 'rxjs';
import { Alumnos } from '../../models/alumnos.models';
import { AlumnosServicioService } from '../../services/alumnos-servicio.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalAlumnoComponent } from '../modal-alumno/modal-alumno.component'

@Component({  
  selector: 'app-listado-alumnos',
  templateUrl: './listado-alumnos.component.html',
  styleUrls: ['./listado-alumnos.component.css']
})
export class ListadoAlumnosComponent implements OnInit {

  lstAlumnos: Observable<Alumnos[]>;  
  mAlumno: Alumnos;

  constructor(private servicioAlumnos: AlumnosServicioService, private modalService: NgbModal ) { }

  ngOnInit(): void {
    this.listarAlumnos();
  }

  listarAlumnos(){
    this.servicioAlumnos.ListarAlumnos().subscribe((alumnos)=>{      
      this.lstAlumnos = alumnos;
      //console.log(this.lstAlumnos);
    },(errorObtenido)=>{
      console.log(errorObtenido);
    });
  }

  verModalAlumno(alumno: Alumnos, tipoCRUD:string){        
    const modalRef = this.modalService.open(ModalAlumnoComponent);
    modalRef.componentInstance.sTipoCRUD = tipoCRUD;
    if(tipoCRUD=="editar"){      
      modalRef.componentInstance.mAlumno = alumno;      
    }else{      
        var nuevoUsuario: Alumnos ;
        nuevoUsuario = { iIdAlumno:0, lTelefono:0,sApellidos:'',sDireccion:'',sDNI:'',sNombres:'',sSexo:'',dFechaNacimiento:null};
        modalRef.componentInstance.mAlumno = nuevoUsuario;
    }
    modalRef.result.then( (resultado) => {
        if(resultado){
          this.ngOnInit();
        }
      }
    ).catch( error=> { ///para revisar el error al cerra el modal con click afuera
      console.log(error);
    });

  }

  


}
