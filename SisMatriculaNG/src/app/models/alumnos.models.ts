
export interface Alumnos{
    iIdAlumno:number ;
    sNombres: string;
    sApellidos: string;
    sDireccion:string;
    lTelefono:number;
    sDNI:string;
    dFechaNacimiento:Date;
    sSexo:string;
}