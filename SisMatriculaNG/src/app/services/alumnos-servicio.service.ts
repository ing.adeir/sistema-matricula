import { Injectable } from '@angular/core';

/* Importacion librerias para los servicios */
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Alumnos } from '../models/alumnos.models';

@Injectable({
  providedIn: 'root'
})
export class AlumnosServicioService {

  /* Metodos para los servicios */  
  constructor(private http: HttpClient) { }

  ListarAlumnos(): Observable<any>{
    var servicioURL = 'http://localhost:90/WebApiMatriculas/api/alumnos/'+'listado';
    return this.http.get(`${servicioURL}`);
  }

  EditarAlumno(mAlumno: Alumnos): Observable<any>{
    var servicioURL = 'http://localhost:90/WebApiMatriculas/api/alumnos/'+'actualizar/'+ mAlumno.iIdAlumno;
    return this.http.put<any>(servicioURL, mAlumno);
  }

  GuardarNuevoAlumno(mAlumno: Alumnos): Observable<any>{
    var servicioURL = 'http://localhost:90/WebApiMatriculas/api/alumnos/' + 'registrar';
    return this.http.post<any>(servicioURL, mAlumno);
  }

  



}
